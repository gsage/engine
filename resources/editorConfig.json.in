{
  "projectManager":
  {
    "defaultProjectImage": "images/blank.png",
    "projectTemplates":
    {
      "blank": {"image": "../../images/blank.png", "name": "Blank project", "description": "Blank project. It won't create any project settings, won't load any default models"},
      "actionRpg": {"image": "../../images/rpg.png", "name": "Action RPG", "description": "Default settings for action RPG project with isometric view. Will set up isometric camera controller, add default models." }
    },
    "templatesFolder": "../Resources/editor/projectTemplates"
  },

  "dataManager":
  {
    "extension": "json",
    "charactersFolder": "characters",
    "levelsFolder": "levels",
    "savesFolder": "templates"
  },

  "logConfig": "log.cfg",
  "startupScript": "scripts/entrypoints/editor.lua",
  "inputHandler": "SDL",
  "systems": [
    "ogre", "3dmovement", "recast", "dynamicStats", "lua"
  ],
  "packager": {
    "deps": [
      "tween",
      "i18n",
      "luafilesystem"
    ]
  },

  "imgui": {
    "renderTargetWhitelist": [
      "Editor"
    ],
    "theme": {
      "colors": {
        "text": "0xff000000",
        "textDisabled": "0xff444444",
        "windowBg": "0xffa0a0a0",
        "childWindowBg": "0xffa0a0a0",
        "popupBg": "0xffa0a0a0",
        "border": "0x33000000",
        "borderShadow": "0x00000000",
        "frameBg": "0xaa2D3A48",
        "frameBgHovered": "0xdd2D3A48",
        "frameBgHovered": "0xdd2D3A48",
        "titleBg": "0xffa5a5a5",
        "titleBgActive": "0xffa6a6a6",
        "titleBgCollapsed": "0xaaa5a5a5",
        "menuBarBg": "0xffa0a0a0",
        "scrollbarBg": "0xff2D3A48",
        "scrollbarGrab": "0xffaaaaaa",
        "scrollbarGrabHovered": "0xff4F8FD4",
        "scrollbarGrabActive": "0xff417BBA",
        "sliderGrab": "0xffaaaaaa",
        "sliderGrabHovered": "0xff4F8FD4",
        "sliderGrabActive": "0xff417BBA",
        "button": "0xffbbbbbb",
        "buttonHovered": "0xff4F91D8",
        "buttonActive": "0xff4F8FD4",
        "header": "0xff4F8FD4",
        "headerHovered": "0xff4F91D8",
        "headerActive": "0xff4F8FD4"
      },
      "windowTitleAlign": "0.5,0.5",
      "windowPadding": "5,5",
      "windowRounding": 5,
      "childRounding": 5,
      "framePadding": "5,5",
      "buttonTextAlign": "0.5,0.5",
      "frameRounding": 4,
      "itemSpacing": "12,8",
      "itemInnerSpacing": "8,6",
      "indentSpacing": 25,
      "scrollbarRounding": 2,
      "scrollbarSize": 20,
      "grabMinSize": 20,
      "grabRounding": 3.0,
      "frameBorderSize": 1
    },
    "fonts": {
      "default": {
        "file": "fonts/Koruri-Regular.ttf",
        "size": 20,
        "oversampleH": 3,
        "oversampleV": 3,
        "glyphRanges": [
          [57344, 60239]
        ]
      },
      "glyph": {
        "merge": true,
        "file": "fonts/MaterialIcons-Regular.ttf",
        "size": 20,
        "oversampleH": 3,
        "oversampleV": 3,
        "noDefaultRanges": true,
        "glyphRanges": [
          [57344, 60239]
        ],
        "glyphOffset": "0,5",
        "pixelSnapH": true
      }
    }
  },

  "windowManager": {
    "type": "SDL"
  },

  "plugins":
  [
    "@PLUGINS_DIRECTORY@/OgrePlugin",
    "@PLUGINS_DIRECTORY@/ImGUIPlugin",
    "@PLUGINS_DIRECTORY@/ImGUIOgreRenderer",
    "@PLUGINS_DIRECTORY@/SDLPlugin",
    "@PLUGINS_DIRECTORY@/RecastNavigationPlugin"
  ],

  "render":
  {
    "renderSystems": [
      {"id": "Metal Rendering Subsystem"},
      {"id": "OpenGL 3+ Rendering Subsystem"},
      {"id": "OpenGL Rendering Subsystem",
        "Fixed Pipeline Enabled": "Yes",
        "RTT Preferred Mode": "FBO"
      }
    ],
    "forward3D": {
      "width": 4,
      "height": 4,
      "numSlices": 5
    },
    "plugins": [
      "RenderSystem_Direct3D11",
      "RenderSystem_GL",
      "RenderSystem_GL3Plus",
      "RenderSystem_Metal",
      "OctreeSceneManager",
      "ParticleFX"
    ],
    "globalResources":
    {
      "General":
      [
        "FileSystem:materials/",
        "FileSystem:programs/"
      ],
      "Rocket":
      [
        "FileSystem:fonts/",
        "FileSystem:ui/"
      ]
    },

    "window":
    {
      "viewport": {
        "backgroundColor": "0xff0a1f2c",
        "renderQueueSequence": [{"id": 101, "suppressShadows": true}]
      },
      "workspaceName": "editor",
      "name": "Editor",
      "width": 1920,
      "height": 1200,
      "useWindowManager": true,
      "resizable": true,
      "params":
      {
        "FSAA": 0,
        "displayFrequency": 50,
        "vsync": false,
        "gamma": "true"
      },
      "defaultCamera": {
        "name": "main"
      }
    }
  }
}
