local icons = {
  error = "",            -- 0xE000
  error_outline = "",    -- 0xE001
  warning = "",          -- 0xE002
  add_alert = "",        -- 0xE003

  equalizer = "",        -- 0xE01D

  pause = "",            -- 0xE034
  play_arrow = "",       -- 0xE037
  stop = "",             -- 0xE047

  dehaze = "",           -- 0xE3C7
  tune = "",             -- 0xE429
  directions_run ="",    -- 0xE566
  chevron_left = "",     -- 0xE5CC
  show_chart = "",       -- 0xE6E1
  rotation_3d = "",      -- 0xE84D
  code = "",             -- 0xE86F
  setting = "",          -- 0xE8B8
  setting_ethernet = "", -- 0xE8BE
  list = "",             -- 0xE896
  timeline = "",         -- 0xE922

  save = "",
  insert_drive_file = "",
  undo = "",
  redo = "",

  to_bottom = "",
  block = "",


  -- TODO: define all supported material icons

}

return icons
